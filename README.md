# Tail Recursion

Tail recursion is a technique to solve the memory runaway and computation time issue with recursion. The example here is to calculate fibonacci number which is a good example.

## How to run
Run the script as follows:
```
$ python3 tail_recursion.py {nth fibonacci number}
```
First it will run the normal recursion and after that it will run `tail recursion`.

It is a great improvement in execution time and memory footprint.

![Example](sample/example1.png)
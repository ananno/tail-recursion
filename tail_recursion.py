# Fibonacci
import sys
import time
import resource


def normal_fib(nth_fib_number):
    mem = get_memory_usage()
    mem1, mem2 = 0, 0
    if nth_fib_number <=1:
        fib = nth_fib_number
    else:
        fib1, mem1 = normal_fib(nth_fib_number-2)
        fib2, mem2 = normal_fib(nth_fib_number-1)
        fib = fib1 + fib2
    
    return fib, max(max(mem, mem1), mem2)

def tail_recursive_fib(nth_fib_number, a=0, b=1):
    mem = get_memory_usage()
    mem1 = 0
    if nth_fib_number == 0:
        fib = a
    elif nth_fib_number == 1:
        fib = b
    else:
        fib, mem1 = tail_recursive_fib(nth_fib_number-1, b, a+b)
    
    return fib, max(mem1, mem)

def get_memory_usage():
    mem = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
    return mem

if __name__ == "__main__":
    nth_number = 30 if len(sys.argv) == 1 else int(sys.argv[1])
    print("[Normal Recursion]\nFirst %d fibonacci number : " % nth_number)
    for n in range(nth_number):
        st = time.time()
        fib1, max_mem = normal_fib(n)
        max_mem = '{:,.0f}'.format(max_mem/float(1<<7))+" kb"
        print("[RAM Usage : {}] : [Time : {:.6f} sec] {}  -> {}".format(max_mem, time.time() - st, n, fib1))

    print("[Tail Recursion]\nFirst %d fibonacci number : " % nth_number)
    for n in range(nth_number):
        st = time.time()
        fib1, max_mem = tail_recursive_fib(n)
        max_mem = '{:,.0f}'.format(max_mem/float(1<<7))+" kb"
        print("[RAM Usage : {}] : [Time : {:.6f} sec] {}  -> {}".format(max_mem, time.time() - st, n, fib1))